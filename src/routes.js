import HelloWorld from './components/HelloWorld.vue'
import Wallets from './components/Wallets.vue'
import Wallet from './components/Wallet.vue'
export const routes = [
    {
        path: '/',
        redirect: '/wallets',
        component: HelloWorld
    },
    {
        path: '/wallets',
        component: {
            render(c) {
                return c('router-view')
            }
        },
        children: [
            {
                path: '',
                component: Wallets
            },
            {
                path: ':name',
                meta: {
                    label: 'Wallet'
                },
                name: 'Wallet',
                component: Wallet
            }
        ]
    }
]