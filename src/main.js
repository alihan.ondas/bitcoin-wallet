import Vue from 'vue'
import VueRouter from 'vue-router'
import { store } from './store'
import App from './App.vue'
import { routes } from './routes'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
