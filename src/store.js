import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const token = process.env.VUE_APP_TOKEN
const tokenQuery = '?token=' + token
axios.defaults.baseURL = process.env.VUE_APP_BITCOIN_URL
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

const getLocalStorageDataIfExists = async (commit, state) => {
    console.log('calling localstorage set', state.wallets)
    if (!state.wallets) {
        commit('setWallets', JSON.parse(localStorage.getItem('wallets')))
        console.log(state.wallets)
    }
}

export const store = new Vuex.Store({
    state: {
        wallets: []
    },
    mutations: {
        setWallets(state, wallets) {
            state.wallets = [...wallets]
            localStorage.setItem('wallets', JSON.stringify(state.wallets))
        },
        addWallet(state, wallet) {
            state.wallets.push(wallet)
            localStorage.setItem('wallets', JSON.stringify(state.wallets))
        },
        setAdresses(state, { walletName, addresses }) {
            console.log('cs', walletName, addresses)

            state.wallets = [...state.wallets.map(c => {
                console.log(c, 'c')
                if (c.name === walletName) {
                    c.addresses = [...addresses]
                }
                return c
            })]
            localStorage.setItem('wallets', JSON.stringify(state.wallets))
        },
        addAddress(state, { walletName, address }) {
            for (let wallet of state.wallets) {
                console.log('check1')
                if (wallet.name === walletName) wallet.addresses.push(address)
                console.log('check2')

            }
            localStorage.setItem('wallets', JSON.stringify(state.wallets))
        },
        deleteWallet(state, walletName) {
            let walletIndex = state.wallets.findIndex(c => c.name === walletName)
            state.wallets.splice(walletIndex, 1)
            localStorage.setItem('wallets', JSON.stringify(state.wallets))
        }
    },
    actions: {
        async requestWallets ({ commit, state }) {
            getLocalStorageDataIfExists(commit, state)

            let { data } = await axios.get(`/wallets${tokenQuery}`)

            commit('setWallets', [...data.wallet_names.map(c => {
                let obj = {
                    name: c,
                    addresses: []
                }
                console.log('inside map func', c, state.wallets)
                for (let wallet of state.wallets) {
                    console.log('let wallet of state.wallets', wallet)
                    if (wallet.name == c) {
                        obj = wallet
                        console.log('in if',obj)

                    }
                }

                return obj
            })])
        },
        async createWallet ({ commit }, walletCreateForm) {

            try {
                // creating one address so that we can create a wallet with it
                let { data: addrsData } = await axios.post(`/addrs`)
            
                let { data: walletData } = await axios.post(`/wallets${tokenQuery}`, {
                    name: walletCreateForm.name,
                    addresses: [addrsData.address]
                })

                commit('addWallet', {
                    name: walletData.name,
                    addresses: [...walletData.addresses.map(c => ({ name: c, balance: '' }))]
                })
            } catch (err) {
                alert('Ошибка: ', err.message)
            }
        },
        async requestAdresses ({ commit, state, dispatch }, walletName) {
            getLocalStorageDataIfExists(commit, state)

            await dispatch('requestWallets')
            
            console.log('check 1')

            const { data } = await axios.get(`/wallets/${walletName}${tokenQuery}`)
            
            const addresses = await Promise.all(data.addresses.map(async c => {
                let { data } = await axios.get(`/addrs/${c}/balance`)
                return {
                    name: data.address,
                    balance: data.balance
                }
            }))

            commit('setAdresses', { walletName, addresses })
        },
        async createAddressInWallet ({ commit }, walletName) {
            const { data } = await axios.post(`/wallets/${walletName}/addresses/generate${tokenQuery}`)
            console.log(data)
            
            const addresses = await Promise.all(data.addresses.map(async c => {
                let { data } = await axios.get(`/addrs/${c}/balance`)
                return {
                    name: data.address,
                    balance: data.balance
                }
            }))

            commit('setAdresses', { walletName, addresses })
        },

        async deleteWallet ({ commit }, walletName) {
            await axios.delete(`/wallets/${walletName}${tokenQuery}`)
            commit('deleteWallet', walletName)
        },

        async addAddress ({ commit }, { walletName, addressName }) {
            console.log('HJSBJSDHFBJSDFHB', walletName, addressName)
            let { data } = await axios.post(`/wallets/${walletName}/addresses${tokenQuery}`, {
                addresses: [addressName]
            })

            const addresses = await Promise.all(data.addresses.map(async c => {
                let { data } = await axios.get(`/addrs/${c}/balance`)
                return {
                    name: data.address,
                    balance: data.balance
                }
            }))

            commit('setAdresses', { walletName, addresses })
        }
    }
})